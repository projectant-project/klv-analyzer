﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KlvLibrary.Utils;

namespace KlvLibrary
{
    class KlvDecoder
    {

        public UInt32 dataSize;
        public double framerate;
        private Queue<Type> complexQueue = new Queue<Type>();
        private Queue<int> complexJump = new Queue<int>();
        private Queue<string> measurementQueue = new Queue<string>();
        private Queue<double> scaleQueue = new Queue<double>();
        private List<DataHolder> datas = new List<DataHolder>();
        private List<ComplexData[]> complexDatas = new List<ComplexData[]>();
        private int indexMaxRep;
        private string sensorName;
        private string deviceName;
        private double timeOffset = 0;
        private double timeSpan = 0;

        public KlvDecoder() {  }

        private UInt32 BytesToUInt32(byte[] buffer, int pos)
        {
            byte[] key = new byte[4];
            Buffer.BlockCopy(buffer, pos, key, 0, 4);
            return BitConverter.ToUInt32(key);
        }

        public void BuildMeasurement(object[] data, UInt32 repeat, UInt32 samplesize)
        {
            measurementQueue.Clear();
            string units = "";

            for (int i = 0; i < repeat; i++, units = "")
            {
                for (int j = 0; j < samplesize; j++)
                {
                    if ((char)data[i * samplesize + j] != '\0')
                        units += (char)data[i * samplesize + j];
                }

                measurementQueue.Enqueue(units);
            }
        }

        private void BuildQueue(object[] data)
        {
            this.complexJump.Clear();
            complexQueue.Clear();
            for (int i = 0; i < data.Length; i++)
            {
                UInt32 type = Convert.ToUInt32(data[i]);
                Type t = GetType(type);

                complexQueue.Enqueue(t);
                complexJump.Enqueue(UnitSize(t));
            }
        }

        private void HandleScale(object data)
        {
            if (data is DataHolder)
                BuildScale((DataHolder)data);
            else
                BuildScale((string)data);
        }

        private void BuildScale(DataHolder holder)
        {
            for (int i = 0; i < holder.FormattedData.Length; i++)
            {
                BuildScale(holder.FormattedData[i]);
            }
        }

        private void BuildScale(string value)
        {
            double scale = Convert.ToDouble(value);
            this.scaleQueue.Enqueue(scale);
        }

        private void HandleComplex(byte[] buffer, UInt32 repeat, UInt32 samplesize, int pos, string keystr, bool sensorData)
        {
            ComplexData[] data = new ComplexData[repeat];
            Type unitType;
            int unitSize;
            int padding; //, jump;
            
            if ((repeat * samplesize) % 4 != 0)
                padding = 4 - ((int)(repeat * samplesize) % 4);
            else
                padding = 0;

            for (UInt32 i = 0; i < repeat; i++)
            {
                data[i] = new ComplexData((UInt32)complexQueue.Count);
            }

            for (UInt32 i = 0; i < repeat; i++)
            {
                for (int j = 0; j < complexQueue.Count; j++, pos += (int)unitSize)
                {
                    // dequeue
                    unitSize = complexJump.Dequeue();
                    unitType = complexQueue.Dequeue();
                    if (unitType != null)
                        data[i].fields[j] = ReadValue(buffer, (int)pos, unitSize, unitType);
                    else
                    {
                        pos -= (int)unitSize;
                        data[i].RemoveLastItem();
                    }

                    // enqueue
                    complexJump.Enqueue(unitSize);
                    complexQueue.Enqueue(unitType);
                }
            }

            complexDatas.Add(data);

            if (sensorData)
            {
                DataHolder holder = new DataHolder(keystr, framerate / complexDatas.Count, (UInt32)complexDatas.Count);
                for (int i = 0; i < complexDatas.Count; i++)
                {
                    string str = "";
                    for (int j = 0; j < complexDatas[i].Length; j++)
                    {
                        str += complexDatas[i][j].ToString();
                    }
                    string newStr;
                    if (str == "")
                        newStr = "NULL";
                    else
                        newStr = str.Remove(str.Length - 2);
                    holder.FormattedData[i] = newStr;
                }
                holder.Name = sensorName;
                datas.Add(holder);
                complexDatas.Clear();
            }

        }

        private void HandlePure(byte[] buffer, UInt32 repeat, UInt32 samplesize, UInt32 type, int pos, string keystr, bool sensorData)
        {
            UInt32 padding;
            Type dataType = GetType(type);
            int sizeToRead = UnitSize(dataType);
            UInt32 dataSize = repeat * samplesize / (UInt32)sizeToRead;
            object[] data = new object[dataSize];
            string formatteddata = "";
            double scale;

            for (int i = 0; i < dataSize; i++, pos += sizeToRead)
            {
                data[i] = ReadValue(buffer, pos, sizeToRead, dataType);
            }

            if ((dataSize * sizeToRead) % 4 != 0)
                padding = 4 - ((UInt32)(dataSize * sizeToRead) % 4);
            else
                padding = 0;

            pos += (int)padding;


            DataHolder holder = new DataHolder(keystr, framerate / repeat, repeat);


            for (int i = 0; i < dataSize; i++)
            {              
                string strData = ObjectToFormattedData(data[i], dataType);
                if (sensorData && measurementQueue.Count > 0)
                {
                    if (scaleQueue.Count > 0)
                    {
                        scale = scaleQueue.Dequeue();
                        strData = ScaleString(strData, scale);
                        scaleQueue.Enqueue(scale);
                    }
                    formatteddata += strData;

                    string measurement = measurementQueue.Dequeue();
                    formatteddata += measurement;
                    measurementQueue.Enqueue(measurement);
                }
                else
                    formatteddata += strData;

                if (dataType != typeof(char) && repeat > 1)
                {
                    if (((i + 1) * (UInt32)sizeToRead) % samplesize == 0)
                    {
                        //Console.Write("{0}   ", formatteddata);
                        holder.FormattedData[((i + 1) * (UInt32)sizeToRead) / samplesize - 1] = formatteddata;
                        formatteddata = "";
                    }
                    else
                        formatteddata += ", ";
                }
            }

            if (sensorData)
            {
                holder.Name = sensorName;
                datas.Add(holder);
                measurementQueue.Clear();
                scaleQueue.Clear();
            }

            if (keystr == "TYPE")
                BuildQueue(data);
            
            if (keystr == "UNIT" || keystr == "SIUN")
                BuildMeasurement(data, repeat, samplesize);

            if (keystr == "STNM")
            {
                if (formatteddata[formatteddata.Length - 1] == 0)
                    formatteddata = formatteddata.Remove(formatteddata.Length - 1);

                sensorName = formatteddata;
            }

            if (keystr == "DVNM")
                deviceName = formatteddata;

            if (keystr == "SCAL")
            {
                if (formatteddata == "")
                    HandleScale(holder);
                else
                    HandleScale(formatteddata);
            }
        }

        private string ScaleString(string data, double scale)
        {
            double dataInt = Convert.ToDouble(data);
            return ((int)dataInt / (int)scale).ToString();
        }

        private object ScaleObject(object data, int scale, Type dataType)
        {
            object obj;
            
            switch (Type.GetTypeCode(dataType))
            {
                case TypeCode.Double:
                    obj = Point2StringFormat((double)data / scale);
                    break;

                case TypeCode.Single:
                    obj = Point2StringFormat((float)data / scale);
                    break;

                case TypeCode.Int32:
                    obj = ((Int32)data / scale).ToString();
                    break;

                case TypeCode.Int16:
                    obj = ((Int16)data / scale).ToString();
                    break;

                case TypeCode.UInt16:
                    obj = ((UInt16)data / scale).ToString();
                    break;

                case TypeCode.SByte:
                    obj = ((sbyte)data / scale).ToString();
                    break;

                case TypeCode.Byte:
                    obj = ((byte)data / scale).ToString();
                    break;

                default:
                    obj = ((int)data / scale).ToString();
                    break;
            }

            return obj;
        }

        private string ObjectToFormattedData(object data, Type dataType)
        {
            string str;

            switch (Type.GetTypeCode(dataType))
            {
                case TypeCode.Char:
                    str = data.ToString();
                    break;

                case TypeCode.Double:
                    str = Point2StringFormat((double)data);
                    break;

                case TypeCode.Single:
                    str = Point2StringFormat((float)data);
                    break;

                case TypeCode.UInt32:
                    str = ((UInt32)data).ToString();
                    break;

                case TypeCode.Int32:
                    str = ((Int32)data).ToString();
                    break;

                case TypeCode.Int16:
                    str = ((Int16)data).ToString();
                    break;

                case TypeCode.UInt16:
                    str = ((UInt16)data).ToString();
                    break;

                case TypeCode.SByte:
                    str = ((sbyte)data).ToString();
                    break;

                case TypeCode.Byte:
                    str = ((byte)data).ToString();
                    break;

                default:
                    str = data.ToString();
                    break;
            }

            return str;
        }

        public static string Point2StringFormat(double x)
        {
            double point = Math.Floor(10000 * x) / 10000;
            string s = point.ToString("N4");
            return s;
        }

        private object ReadValue(byte[] data, int pos, int sampleSize, Type type)
        {
            byte[] value = new byte[sampleSize];
            Buffer.BlockCopy(data, pos, value, 0, sampleSize);
            value = ToLittleEndian(value);

            return GetValue(value, type);
        }

        private object GetValue(byte[] value, Type type)
        {
            object ret;
            double number; string format;

            switch (Type.GetTypeCode(type))
            {
                // fourCC
                case TypeCode.String:
                    value = ToLittleEndian(value);
                    ret = Common.Key2FourCC(BitConverter.ToUInt32(value));
                    break;

                // char
                case TypeCode.Char:
                    ret = (char)value[0];
                    break;

                // single byte signed
                case TypeCode.SByte:
                    ret = (SByte)value[0];
                    break;

                // single byte unsigned
                case TypeCode.Byte:
                    ret = (byte)value[0];
                    break;

                // 16bit integer signed
                case TypeCode.Int16:
                    ret = BitConverter.ToInt16(value);
                    break;

                // 16bit integer unsigned
                case TypeCode.UInt16:
                    ret = BitConverter.ToUInt16(value);
                    break;

                // float
                case TypeCode.Single:
                    number = BitConverter.ToSingle(value);
                    format = Point2StringFormat(number);
                    ret = Convert.ToSingle(format);
                    break;

                // double
                case TypeCode.Double:
                    number = BitConverter.ToDouble(value);
                    format = Point2StringFormat(number);
                    ret = Convert.ToDouble(format);
                    break;

                // 32bit integer signed
                case TypeCode.Int32:
                    ret = BitConverter.ToInt32(value);
                    break;

                // 32bit integer unsigned
                case TypeCode.UInt32:
                default:
                    ret = BitConverter.ToUInt32(value);
                    break;

            }

            return ret;
        }

        private Type GetType(UInt32 type)
        {
            Type ret;
            switch (type)
            {
                // char
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_UTC_DATE_TIME:
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_STRING_ASCII:
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_STRING_UTF8:
                    ret = typeof(char);
                    break;

                // single byte signed
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_SIGNED_BYTE:
                    ret = typeof(SByte);
                    break;

                // single byte unsigned
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_UNSIGNED_BYTE:
                    ret = typeof(Byte);
                    break;

                // 16bit integer signed
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_SIGNED_SHORT:
                    ret = typeof(Int16);
                    break;

                // 16bit integer signed
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_UNSIGNED_SHORT:
                    ret = typeof(UInt16);
                    break;

                // float
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_FLOAT:
                    ret = typeof(float);
                    break;

                // double
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_DOUBLE:
                    ret = typeof(double);
                    break;

                // complex
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_COMPLEX:
                    ret = typeof(object);
                    break;

                // fourCC
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_FOURCC:
                    ret = typeof(string);
                    break;

                // nest
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_NEST:
                    ret = null;
                    break;


                // 32bit integer signed
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_SIGNED_LONG:
                    ret = typeof(Int32);
                    break;

                // 32bit integer unsigned
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_UNSIGNED_LONG:
                default:
                    ret = typeof(UInt32);
                    break;
            }

            return ret;
        }

        private int UnitSize(Type type)
        {
            int unitSize;

            switch (Type.GetTypeCode(type))
            {
                // 16bit short signed and unsigned
                case TypeCode.Int16:
                case TypeCode.UInt16:
                    unitSize = 2;
                    break;

                // 32bit float || 32bit double || 32bit integer signed or unsigned or fourCC
                case TypeCode.String:
                case TypeCode.Double:
                case TypeCode.Single:
                case TypeCode.UInt32:
                case TypeCode.Int32:
                    unitSize = 4;
                    break;

                // char or byte signed and unsigned
                case TypeCode.Char:
                case TypeCode.SByte:
                case TypeCode.Byte:
                default:
                    unitSize = 1;
                    break;
            }

            return unitSize;
        }
        
        private byte[] ToLittleEndian(byte[] data)
        {
            byte[] buffer = new byte[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                buffer[data.Length - 1 - i] = data[i];
            }
            return buffer;
        }

        private bool isSensorData(byte[] buffer, int pos, UInt32 datasize, UInt32 jump)
        {
            if (pos + jump >= datasize)
                return true;

            UInt32 key;
            string keystr;


            key = BytesToUInt32(buffer, pos + (int)jump);
            keystr = Common.Key2FourCC(key);


            if (keystr == "STRM") // if next is STRM
                return true;
            else
                return false;
        }

        private GStream GPMF_ResetState(GStream ms)
        {
            if (ms != null)
            {
                ms.Pos = 0;
                ms.Device_count = 0;
                ms.Device_id = 0;
                ms.Device_name = "";

                return ms;
            }

            return null;
        }

        public UInt32[] BytesToUInt32Array(byte[] buffer, UInt32 size)
        {
            UInt32[] buf = new UInt32[size / 4];
            for (int i = 0; i < size; i += 4)
            {
                buf[i / 4] = BytesToUInt32(buffer, i);
            }

            return buf;
        }

        public GStream GPMF_Init(byte[] buffer, UInt32 datasize)
        {
            if (buffer != null && datasize > 0)
            {
                UInt32 key = BytesToUInt32(buffer, 0);

                if (4 < datasize && key == FourCCKey.GPMF_KEY_DEVICE)
                {

                    GStream ms = new GStream();
                    ms.Buffer = buffer;
                    ms.Buffer_long = datasize;

                    return GPMF_ResetState(ms);
                }
            }

            return null;
        }

        public void DecomposePayload(byte[] buffer, int pos, UInt32 datasize)
        {
            if (pos >= datasize)
                return;

            UInt32 key, length, repeat, type, samplesize, padding; //, jump;
            string keystr;
            char typestr;
            bool sensorData;

            key = BytesToUInt32(buffer, pos);
            pos += 4;
            length = BytesToUInt32(buffer, pos);
            pos += 4;

            type = Common.SampleType(length);
            repeat = Common.SampleRepeat(length);
            samplesize = Common.SampleSize(length);

            keystr = Common.Key2FourCC(key);
            typestr = (char)type;

            if ((UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_NEST == type) // if nested get into
                DecomposePayload(buffer, pos, datasize);
            

            else
            {
          
                if ((repeat * samplesize) % 4 != 0)
                    padding = 4 - ((UInt32)(repeat * samplesize) % 4);
                else
                    padding = 0;

                if (keystr == "DVNM")
                    sensorData = false;
                else
                    sensorData = isSensorData(buffer, pos, datasize, repeat * samplesize + padding);

                if ((UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_COMPLEX == type)
                    HandleComplex(buffer, repeat, samplesize, pos, keystr, sensorData);
                else
                    HandlePure(buffer, repeat, samplesize, type, pos, keystr, sensorData);


                pos += (int)padding + (int)(repeat * samplesize);

                DecomposePayload(buffer, pos, datasize);
            }
        }

        public void printData()
        {
            Console.WriteLine("\n\n");
            for (int i = 0; i < datas.Count; i++)
            {
                Console.WriteLine(" ----- Sensor {0}. Total Data {1} with framerate {2}", datas[i].Key, datas[i].Repeat, datas[i].Framerate);
                for (int j = 0; j < datas[i].Repeat; j++)
                {
                    Console.Write("{0}  ", datas[i].FormattedData[j]);
                }
                Console.WriteLine("\n");
            }
        }

        public void ClearLastPayload()
        {
            complexQueue.Clear();
            complexJump.Clear();
            measurementQueue.Clear();
            datas.Clear();
        }

        public KlvObj CreateObject()
        {
            string key, name, value;
            int pos;
            timeSpan = getTimeSpan();
            KlvObj obj = new KlvObj(timeSpan, deviceName);

            for (int i = 0; i < datas.Count; i++)
            {
                pos = datas[i].Pos;
                key = datas[i].Key;

                if (datas[i].FormattedData.Length > pos)
                {
                    if (timeOffset >= pos * datas[i].Framerate)
                    {
                        value = datas[i].FormattedData[pos];


                        if (value != "")
                        { 
                            name = datas[i].Name;
                            obj.datas.Add(new SensorData(key, name, value));
                        }

                        datas[i].Pos++;
                    }
                }
            }
            obj.timeOffset = timeOffset;
            timeOffset += timeSpan;

            return obj;
        }

        public bool isActiveSensor(string key)
        {
            for (int i = 0; i < datas.Count; i++)
            {
                if (datas[i].Key == key)
                    return true;
            }

            return false;
        }
        
        public void FindMaxRepetition()
        {
            indexMaxRep = 0;
            for (int i = 0; i < datas.Count; i++)
            {
                if (datas[i].FormattedData.Length > datas[indexMaxRep].FormattedData.Length)
                    indexMaxRep = i;
            }
        }

        public int Repetition()
        {
            return datas[indexMaxRep].FormattedData.Length;
        }

        public double getTimeSpan()
        {
            return datas[indexMaxRep].Framerate;            
        }

        public void setFrameRate(double framerate)
        {
            this.framerate = framerate;
        }

        public void setDataSize(UInt32 dataSize)
        {
            this.dataSize = dataSize;
        }
    }
}
