﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlvLibrary.Utils
{
    public class ComplexData
    {
        private UInt32 size;
        public object[] fields;

        public ComplexData(UInt32 size)
        {
            this.size = size;
            fields = new object[size];
        }

        public UInt32 GetSize() { return size; }

        public void RemoveLastItem()
        {
            fields = fields.Take(fields.Count() - 1).ToArray();
            this.size--;
        }

        public override string ToString()
        {
            string str = "";
            Type dataType;
            for (UInt32 i = 0; i < this.size; i++)
            {
                dataType = this.fields[i].GetType();
                if (dataType == typeof(char))
                    str += fields[i].ToString();
                else if (dataType == typeof(double))
                    str += ((double)fields[i]).ToString();
                else if (dataType == typeof(float))
                    str += ((float)fields[i]).ToString();
                else if (dataType == typeof(UInt32))
                    str += ((UInt32)fields[i]).ToString();
                else if (dataType == typeof(Int32))
                    str += ((Int32)fields[i]).ToString();
                else if (dataType == typeof(Int16))
                    str += ((Int16)fields[i]).ToString();
                else if (dataType == typeof(UInt16))
                    str += ((UInt16)fields[i]).ToString();
                else if (dataType == typeof(sbyte))
                    str += ((sbyte)fields[i]).ToString();
                else if (dataType == typeof(byte))
                    str += ((byte)fields[i]).ToString();
                else if (dataType == typeof(string))
                    str += fields[i];

                str += ", ";
            }

            return str;
        }
    }
}
