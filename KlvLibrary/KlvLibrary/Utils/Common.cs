﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace KlvLibrary.Utils
{
    public class Common
    {
		private static Regex r = new Regex("^[a-zA-Z0-9]*$");

		public enum GPMF_ERROR
        {
            GPMF_OK = 0,
			GPMF_ERROR_MEMORY,              // NULL Pointer or insufficient memory 
			GPMF_ERROR_BAD_STRUCTURE,       // Non-complaint GPMF structure detected
			GPMF_ERROR_BUFFER_END,          // reached the end of the provided buffer
			GPMF_ERROR_FIND,                // Find failed to return the requested data, but structure is valid.
			GPMF_ERROR_LAST,                // reached the end of a search at the current nest level
			GPMF_ERROR_TYPE_NOT_SUPPORTED,  // a needed TYPE tuple is missing or has unsupported elements.
			GPMF_ERROR_SCALE_NOT_SUPPORTED, // scaling for an non-scaling type, e.g. scaling a FourCC field to a float.
			GPMF_ERROR_SCALE_COUNT,         // A SCAL tuple has a mismatching element count.
			GPMF_ERROR_UNKNOWN_TYPE,        // Potentially valid data with a new or unknown type.
			GPMF_ERROR_RESERVED             // internal usage
		}

		public enum GPMF_SAMPLETYPE
		{
			GPMF_TYPE_STRING_ASCII = 'c', //single byte 'c' style character string
			GPMF_TYPE_SIGNED_BYTE = 'b',//single byte signed number
			GPMF_TYPE_UNSIGNED_BYTE = 'B', //single byte unsigned number
			GPMF_TYPE_SIGNED_SHORT = 's',//16-bit integer
			GPMF_TYPE_UNSIGNED_SHORT = 'S',//16-bit integer
			GPMF_TYPE_FLOAT = 'f', //32-bit single precision float (IEEE 754)
			GPMF_TYPE_FOURCC = 'F', //32-bit four character tag 
			GPMF_TYPE_SIGNED_LONG = 'l',//32-bit integer
			GPMF_TYPE_UNSIGNED_LONG = 'L', //32-bit integer
			GPMF_TYPE_Q15_16_FIXED_POINT = 'q', // Q number Q15.16 - 16-bit signed integer (A) with 16-bit fixed point (B) for A.B value (range -32768.0 to 32767.99998). 
			GPMF_TYPE_Q31_32_FIXED_POINT = 'Q', // Q number Q31.32 - 32-bit signed integer (A) with 32-bit fixed point (B) for A.B value. 
			GPMF_TYPE_SIGNED_64BIT_INT = 'j', //64 bit signed long
			GPMF_TYPE_UNSIGNED_64BIT_INT = 'J', //64 bit unsigned long	
			GPMF_TYPE_DOUBLE = 'd', //64 bit double precision float (IEEE 754)
			GPMF_TYPE_STRING_UTF8 = 'u', //UTF-8 formatted text string.  As the character storage size varies, the size is in bytes, not UTF characters.
			GPMF_TYPE_UTC_DATE_TIME = 'U', //128-bit ASCII Date + UTC Time format yymmddhhmmss.sss - 16 bytes ASCII (years 20xx covered)
			GPMF_TYPE_GUID = 'G', //128-bit ID (like UUID)

			GPMF_TYPE_COMPLEX = '?', //for sample with complex data structures, base size in bytes.  Data is either opaque, or the stream has a TYPE structure field for the sample.
			GPMF_TYPE_COMPRESSED = '#', //Huffman compression STRM payloads.  4-CC <type><size><rpt> <data ...> is compressed as 4-CC '#'<new size/rpt> <type><size><rpt> <compressed data ...>

			GPMF_TYPE_NEST = 0, // used to nest more GPMF formatted metadata 

			/* ------------- Internal usage only ------------- */
			GPMF_TYPE_EMPTY = 0xfe, // used to distinguish between grouped metadata (like FACE) with no data (no faces detected) and an empty payload (FACE device reported no samples.)
			GPMF_TYPE_ERROR = 0xff // used to report an error
		}

		public enum GPMF_LEVELS
        {
			GPMF_CURRENT_LEVEL,  // search or validate within the current GPMF next level
			GPMF_RECURSE_LEVELS, // search or validate recursing all levels
			GPMF_TOLERANT        // Ignore minor errors like unknown datatypes if the structure is otherwise valid. 
		}

		public static bool IsValidFourCC(string key)
        {
			return r.IsMatch(key);
        }

		public static UInt16 ByteSwap16(UInt16 word)
		{
			return (UInt16)(word << 8 | word >> 8);
		}

		public static UInt32 ByteSwap32(UInt32 word)
		{

			UInt16 little = ByteSwap16((UInt16)(word >> 16));
			UInt16 big = ByteSwap16((UInt16)(word & 0xffff));

			return (((UInt32)big) << 16) | little;
		}

		public static UInt32 MakeKey(string key)
		{
			if (key.Length != 4)
				return 0;

			UInt32 keyRet = 0;

			for (int i = 0; i < 4; i++)
			{
				keyRet += (UInt32)(key[i] & 0xff) << (i * 8);
			}

			return keyRet;
		}

		public static string Key2FourCC(UInt32 key)
		{
			string fourCC = "";
			fourCC += (char)(key & 0xff);
			fourCC += (char)((key >> 8) & 0xff);
			fourCC += (char)((key  >> 16)& 0xff);
			fourCC += (char)((key >> 24) & 0xff);

			return fourCC;
		}

		public static UInt32 SampleType(UInt32 length)
		{
			return length & 0xff;
		}

		public static UInt32 SampleSize(UInt32 length)
		{
			return (length >> 8) & 0xff;
		}

		public static UInt32 SampleRepeat(UInt32 length)
		{
			UInt16 repeat = (UInt16)(length >> 16);
			return ByteSwap16(repeat);
		}

		public static UInt32 DataSize(UInt32 length)
		{
			return SampleSize(length) * SampleRepeat(length);
		}
	}
}
