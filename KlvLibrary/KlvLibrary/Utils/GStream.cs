﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KlvLibrary.Utils
{
    public class GStream
    {
        private string device_name;
        private UInt32 device_id;
        private UInt32 device_count;
        private byte[] buffer;
        private UInt32 buffer_long;
        private UInt32 pos;

        public GStream()
        {

        }
        
        public string Device_name { get => device_name; set => device_name = value; }
        public uint Device_id { get => device_id; set => device_id = value; }
        public uint Device_count { get => device_count; set => device_count = value; }
        public byte[] Buffer { get => buffer; set => buffer = value; }
        public uint Buffer_long { get => buffer_long; set => buffer_long = value; }
        public uint Pos { get => pos; set => pos = value; }
    }
}
