﻿//using System;
//using System.Collections.Generic;
//using System.Runtime.ExceptionServices;
//using System.Text;

//namespace KlvLibrary.Utils
//{
//    public class Utils
//    {
//        public static UInt16 ByteSwap16(UInt16 word)
//        {
//            return (UInt16)(word << 8 | word >> 8);
//        }

//        public static UInt32 ByteSwap32(UInt32 word)
//        {
//            UInt32 ret;
//            //ret = (word & 0xff) << 24;
//            //ret |= (word & 0xff00) << 8;
//            //ret |= (word & 0xff0000) >> 8;
//            //ret |= (word & 0xff000000) >> 24;

//            ret = (word & 0xff) << 24;
//            ret |= (word & 0xff00) << 8;
//            ret |= (word >> 8) & 0xff00;
//            ret |= (word >> 24) & 0xff;

//            return ret;
//        }

//        public static UInt32 MakeKey(string key)
//        {
//            if (key.Length != 4)
//                return 0;

//            UInt32 keyRet = 0;

//            for (int i = 0; i < 4; i++)
//            {
//                keyRet += (UInt32)(key[i] & 0xff) << (i * 8);
//            }

//            return keyRet;
//        }
//    }
//}
