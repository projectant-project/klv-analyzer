﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KlvLibrary.Utils
{
    public class SensorData
    {
        public string key { get; set; }
        public string name { get; set; }
        public string data { get; set; }
        
        public SensorData(string key, string name, string data)
        {
            this.key = key;
            this.name = name;
            this.data = data;
        }
    }
}
