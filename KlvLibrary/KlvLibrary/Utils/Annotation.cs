﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KlvLibrary.Utils
{
    public class Annotation
    {
        public string key { get; set; }
        public string data { get; set; }

        public Annotation(string key, string data)
        {
            this.key = key;
            this.data = data;
        }
    }
}
