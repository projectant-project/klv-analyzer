﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KlvLibrary.Utils
{
    class DataHolder
    {
        private string[] formattedData;
        private UInt32 repeat;
        private double framerate;
        private string key;
        private string name;
        private int pos;

        public DataHolder(string key, double framerate, UInt32 repeat)
        {
            Pos = 0;
            this.key = key;
            this.framerate = framerate;
            this.repeat = repeat;

            formattedData = new string[repeat];
        }

        public string[] FormattedData { get => formattedData; set => formattedData = value; }
        public string Key { get => key; set => key = value; }
        public double Framerate { get => framerate; set => framerate = value; }
        public UInt32 Repeat { get => repeat; set => repeat = value; }
        public string Name { get => name; set => name = value; }
        public int Pos { get => pos; set => pos = value; }
    }
}
