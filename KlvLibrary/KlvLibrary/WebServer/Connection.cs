﻿using KlvLibrary.Utils;
using KlvLibrary.Settings;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Fleck;
using KlvLibrary.InfluxDBManager;

namespace KlvLibrary.WebServer
{
    public class Connection
    {
        private KlvConsumer consumer;
        private IWebSocketConnection socket;
        private InfluxDBExecute dBExecute;
        private string device;
        private string deviceToken = "DEVICE:";
        private string annotationToken = "ANNOTATION:";

        public Connection(IWebSocketConnection socket)
        {
            Console.WriteLine("client connecting");
            this.socket = socket;
            this.socket.OnMessage = (message) => this.OnMessage(message);
            this.socket.OnClose = () => OnClose();
        }

        public void HandleKlv()
        {
            string path = $"{GlobalSettings.path}/{this.device}/{this.device}.mp4";
            this.consumer = new KlvConsumer(path, this.device, this.dBExecute);
            //this.consumer.KlvFinished += this.SendObj;
            this.consumer.CreateKLVObjectsFromFile();
        }

        public async void GetAnnotations(double offset)
        {
            DateTime date = await this.dBExecute.GetAnnotationByOffset(offset);
            //Dictionary<string, string> data = await this.dBExecute.ReadAllAnotations(date);
            //Console.WriteLine(date.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            List <Annotation> data = await this.dBExecute.ReadAllAnotations(date);

            AnnotationObj obj = new AnnotationObj(data);
            //Console.WriteLine(obj.ToString());
            _ = this.socket.Send(obj.ToString());
        }


        public void SendObj(object sender, KlvEventArg obj)
        {
            this.socket.Send(obj.Obj);
        }

        public void OnMessage(string message)
        {
            //Console.WriteLine(message);
            if (message.StartsWith("DEVICE:"))
            {
                this.device = message.Substring(this.deviceToken.Length);
                Console.WriteLine($"Got Device:{this.device}");
                this.dBExecute = new InfluxDBExecute(this.device);
                HandleKlv();
            }
            else
            {
                string offset = message.Substring(this.annotationToken.Length);
                //Console.WriteLine($"Got Annotate:{offset}");
                GetAnnotations(Convert.ToDouble(offset));
            }
        }

        public void OnClose()
        {
            Console.WriteLine("Connection Closed !");
            this.socket.Close();      
        }
    }
}
