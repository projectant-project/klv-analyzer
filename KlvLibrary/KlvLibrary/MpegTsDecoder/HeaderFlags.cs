﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KlvLibrary.MpegTsDecoder
{
    public class HeaderFlags
    {

        public enum AdaptionFieldControl
        {
            RESERVED_FOR_FUTURE = 0,
            NO_ADAPTION_ONLY_PAYLOAD,
            ADAPTION_ONLY_NO_PAYLOAD,
            ADAPTION_AND_PAYLOAD 
        };
    }
}
