﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KlvLibrary.MpegTsDecoder
{
    public class AdaptionField
    {
        public byte adaptionFieldLength { get; set; }
        public bool discontinuityIndicator { get; set; }
        public bool randomAccessIndicator { get; set; }
        public bool esPriorityIndicator { get; set; }
        public bool pcrFlag { get; set; }
        public bool opcrFlag { get; set; }
        public bool splicingFlag { get; set; }
        public bool trsportPrivateDataFlag { get; set; }
        public bool adaptionFieldExtraFlag { get; set; }

        public AdaptionField() { }

        public void setHeader(byte[] headerb)
        {
            adaptionFieldLength =       headerb[0];
            discontinuityIndicator =    ((headerb[1] & 0x80) >> 7 == 1) ? true : false;
            randomAccessIndicator =     ((headerb[1] & 0x40) >> 6 == 1) ? true : false;
            esPriorityIndicator =       ((headerb[1] & 0x20) >> 5 == 1) ? true : false;
            pcrFlag =                   ((headerb[1] & 0x10) >> 4 == 1) ? true : false;
            opcrFlag =                  ((headerb[1] & 0x08) >> 3 == 1) ? true : false;
            splicingFlag =              ((headerb[1] & 0x04) >> 2 == 1) ? true : false;
            trsportPrivateDataFlag =    ((headerb[1] & 0x02) >> 1 == 1) ? true : false;
            adaptionFieldExtraFlag =    ((headerb[1] & 0x01) == 1) ? true : false;
        }

        public override string ToString()
        {
            return String.Format("adaptionFieldLength -> {0}\n" +
                    "discontinuityIndicator -> {1}\n" +
                    "randomAccessIndicator -> {2}\n" +
                    "esPriorityIndicator -> {3}\n" +
                    "pcrFlag -> {4}\n" +
                    "opcrFlag -> {5}\n" +
                    "splicingFlag -> {6}\n" +
                    "trsportPrivateDataFlag -> {7}\n" +
                    "adaptionFieldExtraFlag -> {8}",
                    adaptionFieldLength, discontinuityIndicator, randomAccessIndicator, esPriorityIndicator,
                    pcrFlag, opcrFlag, splicingFlag, trsportPrivateDataFlag, adaptionFieldExtraFlag);
        }
    }
}
