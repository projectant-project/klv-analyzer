﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using KlvLibrary.Utils;
using KlvLibrary.InfluxDBManager;

namespace KlvLibrary
{
    public class KlvConsumer
    {
        public static bool DEBUD = false;
        //public event EventHandler<KlvEventArg> KlvFinished;
        private string path;
        private string device;
        private Mp4Object mp4;
        private KlvDecoder klvDecoder;
        private InfluxDBExecute dBExecute;

        public KlvConsumer(string path, string device, InfluxDBExecute dBExecute)
        {
            this.path = path;
            this.device = device;
            this.dBExecute = dBExecute;
        }

        public bool CreateKLVObjectsFromFile()
        {
            OpenFile opener = new OpenFile();
            if (this.path.EndsWith(".mp4"))
            {
                try
                {
                    this.mp4 = opener.openMP4(this.path, Common.MakeKey("meta"), Common.MakeKey("gpmd"));
                }
                catch(System.IO.IOException)
                {
                    Console.WriteLine("Another Client has opened this file. Try again in few seconds");
                    this.mp4 = null;
                }
            }
            else
            {
                mp4 = opener.openUDTA(this.path);
            }
            
            

            if (mp4 == null)
            {
                Console.WriteLine("error: {0} is an invalid MP4/MOV or it has no GPMF data\n\n", path);
                return false;
            }

            Console.WriteLine(mp4.ToString());

            double metadatalength = mp4.GetDuration();
            Console.WriteLine("metadatalength -> {0}", metadatalength);

            if (metadatalength > 0.0)
            {
                handleTelemetry();
            }

            Console.WriteLine("Closing File");
            mp4.fs.Close();
            return true;
        }

        //protected virtual void OnKlvFinished(KlvEventArg obj)
        //{
        //    KlvFinished?.Invoke(this, obj);
        //}

        private void handleTelemetry()
        {
            UInt32 payloadsNum = mp4.GetNumberPayloads();
            UInt32 frames = mp4.GetFramesNumber();
            double fps = mp4.GetFps();
            GStream ms;

            //this.dBExecute = new InfluxDBExecute(this.device);
            klvDecoder = new KlvDecoder();
            Console.WriteLine($"set frame rate as : {mp4.basemetadataduration / (double)mp4.meta_clockdemon}");
            klvDecoder.setFrameRate(mp4.basemetadataduration / (double)mp4.meta_clockdemon);
            Console.WriteLine("VIDEO FRAMERATE: {0} with {1} frames\nPAYLOADS: {2}", fps, frames, payloadsNum);

            for (int i = 0; i < payloadsNum; i++)
            {
                ms = generateGstream(i);

                if (ms != null)
                {
                    Payload2KlvObject(ms);
                }
            }
        }

        private GStream generateGstream(int i)
        {
            GStream ms;
            if (DEBUD) Console.WriteLine("payload -> {0}", i);
            UInt32 payloadsize = mp4.GetPayloadSize(i);
            if (DEBUD) Console.WriteLine("payload size -> {0}", payloadsize);

            byte[] payload = mp4.GetPayload(i);
            if (payload == null)
            {
                Console.WriteLine("payload {0} is null", i);
                return null;
            }

            ms = klvDecoder.GPMF_Init(payload, payloadsize);
            if (ms == null)
            {
                Console.WriteLine("can not init pyaload {0}", i);
                return null;
            }

            return ms;
        }

        private void Payload2KlvObject(GStream ms)
        {
            if (DEBUD) Console.WriteLine("GPMF STRUCTURE:");
            klvDecoder.ClearLastPayload();
            klvDecoder.setDataSize(ms.Buffer_long);
            klvDecoder.DecomposePayload(ms.Buffer, 0, ms.Buffer_long);

            klvDecoder.FindMaxRepetition();
            int rep = klvDecoder.Repetition();
            KlvObj lastSensors = null;
            List<SensorData> lastObj = new List<SensorData>();
            KlvObj syncObj;
            //string json;

            for (int j = 0; j < rep; j++)
            {
                syncObj = klvDecoder.CreateObject();
                if (!syncObj.isEqualTo(lastObj))
                {
                    lastObj = syncObj.CopyList();
                    syncObj.RebuildObject(lastSensors);
                    lastSensors = syncObj.UpdateObject(lastSensors);
                    //json = syncObj.ToString();
                    //OnKlvFinished(new KlvEventArg(json));
                    dBExecute.WriteKlvObject(syncObj);
                }
                //obj = syncObj.datas;
            }
        }

    }
}
